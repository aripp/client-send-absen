<?php

// config 
$host  = 'localhost';
$port  = '3306';
$database  = 'absen';
$table  = 'att_log';
$user  = 'root';
$pass = 'toor';

//pin karyawan di tabel absen
$pinKaryawan = ['3020073', '3020074','3021174'];

date_default_timezone_set("Asia/Jakarta");
$tahun = date('Y');
$bulan = date('m');
$hari = date('d');
$pin = implode(",", $pinKaryawan);

$sql = "SELECT * 
        FROM $table 
        WHERE pin IN ($pin) AND 
            DAY(scan_date)='$hari' AND 
            MONTH(scan_date)='$bulan' AND 
            YEAR(scan_date)='$tahun'";

try {
    $koneksi = new PDO("mysql:host=$host;port=$port;dbname=$database;",$user,$pass);
    $koneksi->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
    $row = $koneksi->prepare($sql);
    $row->execute();
    $hasil = $row->fetchAll();
    $data =[];
    foreach($hasil as $isi){
        $data['data'][] = [
            'sn' => $isi['sn'],
            'scan_date' => date("Y-m-d H:i:s", strtotime($isi['scan_date'])),
            'pin' => $isi['pin'],
            'verifymode' => $isi['verifymode'],
            'inoutmode' => $isi['inoutmode'],
            'device_ip' => $isi['device_ip'],
        ];
    }
    postData($data);

}catch (Exception $e) {
    print "Error : " . $e->getMessage() . "<br/>";
    die();
}

function postData($data){
    $url = 'http://1233123.xyz/api/absen-klaten';
    $headers = [
        "x-username : ",
        "x-token : ",
        "content-type: application/json",
    ];
    $payload = json_encode($data);

    var_dump($payload);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    curl_close($ch);
    echo $result;
}
